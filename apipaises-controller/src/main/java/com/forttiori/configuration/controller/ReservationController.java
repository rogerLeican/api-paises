package com.forttiori.configuration.controller;

import com.forttiori.configuration.persistence.dto.ReservationDTO;
import com.forttiori.configuration.persistence.entity.Reservation;
import com.forttiori.configuration.service.reservations.ReservationServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/paises/reservas")
@AllArgsConstructor
public class ReservationController {

    private final ReservationServiceImpl reservationService;

    @PostMapping
    public Reservation save(@RequestBody @Valid ReservationDTO reservationDTO) {
        return this.reservationService.save(reservationDTO);
    }

    @GetMapping
    public List<Reservation> findAll() {
        return this.reservationService.findAll();
    }

    public Optional<Reservation> findByID(@PathVariable String id) {
        return this.reservationService.findByID(id);
    }

    @PutMapping(value = "/{id}")
    public Reservation update(@PathVariable String id, @RequestBody @Valid ReservationDTO reservationDTO) {
        return this.reservationService.update(id, reservationDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable String id) {
        this.reservationService.delete(id);
    }
}
