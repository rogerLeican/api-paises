package com.forttiori.configuration.controller;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.service.countries.PaisServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/paises")
@AllArgsConstructor
public class PaisController {

    private final PaisServiceImpl paisService;

    @GetMapping("/capital/{capital}")
    public List<PaisInfoResponse> getPaisByCapital(@PathVariable String capital) {
         return this.paisService.getPaisByCapital(capital);
    }

    @GetMapping("/name/{name}")
    public List<PaisInfoResponse> getPaisByName(@PathVariable String name) {
        return this.paisService.getPaisByName(name);
    }

    @GetMapping("/region/{region}")
    public List<PaisInfoResponse> getPaisByRegiao(@PathVariable String region) {
        return this.paisService.getPaisByRegiao(region);
    }

    @GetMapping
    public List<PaisInfoResponse> getTodosPaises() {
        return this.paisService.getTodosPaises();
    }
}
