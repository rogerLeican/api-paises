package com.forttiori.configuration.controller.exceptions;

import com.forttiori.configuration.service.exceptions.CapitalNaoExisteException;
import com.forttiori.configuration.service.exceptions.PaisNaoEncontradoException;
import com.forttiori.configuration.service.exceptions.RegiaoNaoEncontradaException;
import com.forttiori.configuration.service.exceptions.ReservaNaoEncontradaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(CapitalNaoExisteException.class)
    public ResponseEntity<StandardError> capitalNaoExiste(CapitalNaoExisteException e) {
        StandardError error = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage(), OffsetDateTime.now());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(PaisNaoEncontradoException.class)
    public ResponseEntity<StandardError> paisNaoEncontrado(PaisNaoEncontradoException e) {
        StandardError error = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage(), OffsetDateTime.now());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }
    @ExceptionHandler(RegiaoNaoEncontradaException.class)
    public ResponseEntity<StandardError> regiaoNaoEncontrada(RegiaoNaoEncontradaException e) {
        StandardError error = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage(), OffsetDateTime.now());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(ReservaNaoEncontradaException.class)
    public ResponseEntity<StandardError> regiaoNaoEncontrada(ReservaNaoEncontradaException e) {
        StandardError error = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage(), OffsetDateTime.now());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }



}
