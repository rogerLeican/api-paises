package com.forttiori.configuration.service.exceptions;

public class RegiaoNaoEncontradaException extends RuntimeException{

    public RegiaoNaoEncontradaException(String message) {
        super(message);
    }
}
