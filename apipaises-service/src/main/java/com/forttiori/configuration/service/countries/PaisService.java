package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PaisService {

    public List<PaisInfoResponse> getPaisByCapital(String capital);
    public List<PaisInfoResponse> getPaisByName(String name);
    public List<PaisInfoResponse> getPaisByRegiao(String region);
    public List<PaisInfoResponse> getTodosPaises();
}
