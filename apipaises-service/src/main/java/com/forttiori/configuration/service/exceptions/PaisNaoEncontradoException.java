package com.forttiori.configuration.service.exceptions;

public class PaisNaoEncontradoException extends RuntimeException {

    public PaisNaoEncontradoException(String message) {
        super(message);
    }
}
