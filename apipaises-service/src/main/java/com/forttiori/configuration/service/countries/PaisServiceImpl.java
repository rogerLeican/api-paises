package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.integration.service.PaisServiceIntegration;
import com.forttiori.configuration.service.exceptions.CapitalNaoExisteException;
import com.forttiori.configuration.service.exceptions.PaisNaoEncontradoException;
import com.forttiori.configuration.service.exceptions.RegiaoNaoEncontradaException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class PaisServiceImpl implements PaisService{

    private final PaisServiceIntegration paisServiceIntegration;

    @Override
    public List<PaisInfoResponse> getPaisByCapital(String capital) {
        try{
            return this.paisServiceIntegration.getPaisByCapital(capital);
        } catch (RuntimeException e) {
            throw new CapitalNaoExisteException("CAPITAL NÃO ENCONTRADA!");
        }
    }

    @Override
    public List<PaisInfoResponse> getPaisByName(String name) {
        try {
            return this.paisServiceIntegration.getPaisByName(name);
        } catch (RuntimeException e) {
            throw new PaisNaoEncontradoException("PAÍS NÃO ENCONTRADO!");
        }
    }

    @Override
    public List<PaisInfoResponse> getPaisByRegiao(String region) {
        try {
            return this.paisServiceIntegration.getPaisByRegiao(region);
        } catch (RuntimeException e) {
            throw new RegiaoNaoEncontradaException("REGIÃO NÃO ENCONTRADA!");
        }
    }

    @Override
    public List<PaisInfoResponse> getTodosPaises() {
        return this.paisServiceIntegration.getTodosPaises();
    }
}
