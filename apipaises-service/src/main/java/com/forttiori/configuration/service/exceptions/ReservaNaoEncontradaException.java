package com.forttiori.configuration.service.exceptions;

public class ReservaNaoEncontradaException extends RuntimeException {

    public ReservaNaoEncontradaException(String message) {
        super(message);
    }
}
