package com.forttiori.configuration.service.exceptions;

public class CapitalNaoExisteException extends RuntimeException{

    public CapitalNaoExisteException(String message) {
        super(message);
    }
}
