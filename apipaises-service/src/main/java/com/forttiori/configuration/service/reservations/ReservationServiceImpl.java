package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.persistence.dto.ReservationDTO;
import com.forttiori.configuration.persistence.entity.Reservation;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.countries.PaisServiceImpl;
import com.forttiori.configuration.service.exceptions.ReservaNaoEncontradaException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    private ReservationRepository reservationRepository;
    private PaisServiceImpl paisService;

    @Override
    public Reservation save(ReservationDTO reservationDTO) {
        this.paisService.getPaisByCapital(reservationDTO.getCapital());
        this.paisService.getPaisByName(reservationDTO.getName());
        this.paisService.getPaisByRegiao(reservationDTO.getRegion());
        return this.reservationRepository.save(
                new Reservation(reservationDTO.getName(),
                        reservationDTO.getRegion(),
                        reservationDTO.getCapital(),
                        reservationDTO.getDate()));
    }

    @Override
    public List<Reservation> findAll() {
        return this.reservationRepository.findAll();
    }

    @Override
    public Optional<Reservation> findByID(String id) {
        try {
            return this.reservationRepository.findById(id);
        } catch (RuntimeException e) {
            throw new ReservaNaoEncontradaException("RESERVA NÃO ENCONTRADA!");
        }
    }

    @Override
    public Reservation update(String id, ReservationDTO reservationDTO) {
        this.paisService.getPaisByRegiao(reservationDTO.getRegion());
        this.paisService.getPaisByName(reservationDTO.getName());
        this.paisService.getPaisByCapital(reservationDTO.getCapital());

        Optional<Reservation> reservation = this.reservationRepository.findById(id);
        reservation.get().setName(reservationDTO.getName());
        reservation.get().setCapital(reservationDTO.getCapital());
        reservation.get().setRegion(reservationDTO.getRegion());
        reservation.get().setDate(reservationDTO.getDate());

        return this.reservationRepository.save(reservation.get());
    }

    @Override
    public void delete(String id) {
        try {
            this.reservationRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new ReservaNaoEncontradaException("RESERVA NÃO ENCONTRADA!");
        }
    }

}
