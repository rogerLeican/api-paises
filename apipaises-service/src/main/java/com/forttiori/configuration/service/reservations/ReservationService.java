package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.persistence.dto.ReservationDTO;
import com.forttiori.configuration.persistence.entity.Reservation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ReservationService {

    public Reservation save(ReservationDTO reservationDTO);
    public List<Reservation> findAll();
    public Optional<Reservation> findByID(String id);
    public Reservation update(String id, ReservationDTO reservationDTO);
    public void delete(String id);
}
