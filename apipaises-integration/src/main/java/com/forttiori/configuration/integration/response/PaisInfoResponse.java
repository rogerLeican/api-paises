package com.forttiori.configuration.integration.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaisInfoResponse {

    private String name;
    private String region;
    private String capital;
}
